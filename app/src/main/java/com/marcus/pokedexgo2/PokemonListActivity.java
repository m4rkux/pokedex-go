package com.marcus.pokedexgo2;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.marcus.pokedexgo2.database.PokemonDAO;
import com.marcus.pokedexgo2.pokemon.Pokemon;
import com.marcus.pokedexgo2.pokemon.PokemonContent;
import com.marcus.pokedexgo2.pokemon.PokemonType;

import java.util.List;
import java.util.zip.Inflater;

/**
 * An activity representing a list of Pokemons. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link PokemonDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class PokemonListActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    PokemonContent pokemonContent;
    RecyclerView.Adapter adapter;
    String orderBy;
    boolean asc = false;
    private DrawerLayout drawer;

    /** constantes do menu ***/
    final static int NAV_CP_HP = 1;
    final static int NAV_ATK_DEF = 2;
    final static int NAV_CANDIES = 3;
    final static int NAV_KMEGG = 4;

    private int showLayout = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme_NoActionBar);
        setContentView(R.layout.activity_pokemon_list);

        pokemonContent = new PokemonContent(getApplicationContext());


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
        toolbar.setNavigationIcon(R.drawable.ic_menu_white_24dp);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);





        View recyclerView = findViewById(R.id.pokemon_list);
        assert recyclerView != null;
        adapter = setupRecyclerView((RecyclerView) recyclerView);

        MobileAds.initialize(getApplicationContext(), getString(R.string.app_id));

        AdView mAdView1 = (AdView) findViewById(R.id.adView1);
        AdRequest adRequest1 = new AdRequest.Builder().build();
        mAdView1.loadAd(adRequest1);
    }

    private RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> setupRecyclerView(@NonNull RecyclerView recyclerView) {
        SimpleItemRecyclerViewAdapter adapter = new SimpleItemRecyclerViewAdapter(pokemonContent.ITEMS);
        recyclerView.setAdapter(adapter);
        return adapter;
    }

    public class SimpleItemRecyclerViewAdapter
            extends RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder> {

        private final List<Pokemon> mValues;

        public SimpleItemRecyclerViewAdapter(List<Pokemon> items) {
            mValues = items;
        }

        LinearLayout numberView = (LinearLayout) findViewById(R.id.viewNumber);
        TextView idTitulo = (TextView) findViewById(R.id.idTitulo);
        ImageView imgArrowNumber = (ImageView) findViewById(R.id.arrowNumber);

        LinearLayout nameView = (LinearLayout) findViewById(R.id.viewName);
        TextView nameTitulo = (TextView) findViewById(R.id.nameTitulo);
        ImageView imgArrowName = (ImageView) findViewById(R.id.arrowName);

        LinearLayout value1View = (LinearLayout) findViewById(R.id.viewValue1);
        TextView value1Titulo = (TextView) findViewById(R.id.value1Titulo);
        ImageView imgArrowMaxCp = (ImageView) findViewById(R.id.arrowMaxCp);

        LinearLayout value2View = (LinearLayout) findViewById(R.id.viewValue2);
        TextView value2Titulo = (TextView) findViewById(R.id.value2Titulo);
        ImageView imgArrowMaxHp = (ImageView) findViewById(R.id.arrowMaxHp);

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.pokemon_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText("#"+Long.toString(mValues.get(position).number));
            holder.mNameView.setText(mValues.get(position).name);

            holder.mImageEgg.setVisibility(View.GONE);
            holder.mListValue2View.setVisibility(View.VISIBLE);


            if(showLayout == NAV_CP_HP) {
                holder.mListValue1View.setText(Integer.toString(mValues.get(position).maxCp));
                holder.mListValue2View.setText(Integer.toString(mValues.get(position).maxHp));

            }
            else if(showLayout == NAV_ATK_DEF) {
                holder.mListValue1View.setText(Integer.toString(mValues.get(position).baseAtk));
                holder.mListValue2View.setText(Integer.toString(mValues.get(position).baseDef));
            }
            else if(showLayout == NAV_CANDIES) {
                if(mValues.get(position).total_candies > 0) {
                    holder.mListValue1View.setText(Integer.toString(mValues.get(position).total_candies));
                }
                else {
                    holder.mListValue1View.setText("0");
                }
                holder.mListValue2View.setText(Integer.toString(mValues.get(position).maxCp));
            }
            else if(showLayout == NAV_KMEGG) {
                if(mValues.get(position).kmEgg != null) {
                    holder.mListValue1View.setText(Integer.toString(mValues.get(position).kmEgg));
                    holder.mImageEgg.setVisibility(View.VISIBLE);
                    holder.mListValue2View.setVisibility(View.GONE);
                }
                else {
                    mValues.remove(position);
                }

            }

            PokemonType tipo = PokemonType.getByNumber(mValues.get(position).type1);
            holder.mView.setBackgroundColor(Color.parseColor(tipo.getLightColor()));

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Context context = v.getContext();
                    Intent intent = new Intent(context, PokemonDetailActivity.class);
                    intent.putExtra(PokemonDetailFragment.ARG_ITEM_ID, Integer.toString(holder.mItem.number));

                    context.startActivity(intent);
                }
            });

            numberView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(showLayout == NAV_CANDIES) {
                        orderByColumnWhere(Pokemon.Colunas.EVOLUTION+" IS NULL AND "+Pokemon.Colunas.CAPTURERATE+" > 0", Pokemon.Colunas.NUMBER);
                    }
                    else if(showLayout == NAV_KMEGG) {
                        orderByColumnWhere(Pokemon.Colunas.KMEGG+" IS NOT NULL", Pokemon.Colunas.NUMBER);
                    }
                    else {
                        orderByColumn(Pokemon.Colunas.NUMBER);
                    }

                    hideArrows();
                    if(asc) {
                        imgArrowNumber.setImageResource(R.drawable.ic_keyboard_arrow_up_white_24dp);
                    }
                    else {
                        imgArrowNumber.setImageResource(R.drawable.ic_keyboard_arrow_down_white_24dp);
                    }
                    imgArrowNumber.setVisibility(View.VISIBLE);
                }
            });

            nameView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(showLayout == NAV_CANDIES) {
                        orderByColumnWhere(Pokemon.Colunas.EVOLUTION+" IS NULL AND "+Pokemon.Colunas.CAPTURERATE+" > 0", Pokemon.Colunas.NAME);
                    }
                    else if(showLayout == NAV_KMEGG) {
                        orderByColumnWhere(Pokemon.Colunas.KMEGG+" IS NOT NULL", Pokemon.Colunas.NAME);
                    }
                    else {
                        orderByColumn(Pokemon.Colunas.NAME);
                    }

                    hideArrows();
                    if(asc) {
                        imgArrowName.setImageResource(R.drawable.ic_keyboard_arrow_up_white_24dp);
                    }
                    else {
                        imgArrowName.setImageResource(R.drawable.ic_keyboard_arrow_down_white_24dp);
                    }
                    imgArrowName.setVisibility(View.VISIBLE);
                }
            });

            value1View.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if(showLayout == NAV_CP_HP) {
                        orderByColumn(Pokemon.Colunas.MAXCP);
                    }
                    else if(showLayout == NAV_ATK_DEF) {
                        orderByColumn(Pokemon.Colunas.BASEATK);
                    }
                    else if(showLayout == NAV_CANDIES) {
                        orderByColumnWhere(Pokemon.Colunas.EVOLUTION+" IS NULL AND "+Pokemon.Colunas.CAPTURERATE+" > 0", Pokemon.Colunas.TOTAL_CANDIES);
                    }
                    else if(showLayout == NAV_KMEGG) {
                        orderByColumnWhere(Pokemon.Colunas.KMEGG+" IS NOT NULL", Pokemon.Colunas.KMEGG);
                    }


                    hideArrows();
                    if(asc) {
                        imgArrowMaxCp.setImageResource(R.drawable.ic_keyboard_arrow_up_white_24dp);
                    }
                    else {
                        imgArrowMaxCp.setImageResource(R.drawable.ic_keyboard_arrow_down_white_24dp);
                    }
                    imgArrowMaxCp.setVisibility(View.VISIBLE);
                }
            });

            value2View.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(showLayout == NAV_CP_HP) {
                        orderByColumn(Pokemon.Colunas.MAXHP);
                    }
                    else if(showLayout == NAV_ATK_DEF) {
                        orderByColumn(Pokemon.Colunas.BASEDEF);
                    }
                    else if(showLayout == NAV_CANDIES) {
                        orderByColumnWhere(Pokemon.Colunas.EVOLUTION+" IS NULL AND "+Pokemon.Colunas.CAPTURERATE+" > 0", Pokemon.Colunas.MAXCP);
                    }
                    else if(showLayout == NAV_KMEGG) {
                        orderByColumnWhere(Pokemon.Colunas.KMEGG+" IS NOT NULL", Pokemon.Colunas.RARITY);
                    }

                    hideArrows();
                    if(asc) {
                        imgArrowMaxHp.setImageResource(R.drawable.ic_keyboard_arrow_up_white_24dp);
                    }
                    else {
                        imgArrowMaxHp.setImageResource(R.drawable.ic_keyboard_arrow_down_white_24dp);
                    }
                    imgArrowMaxHp.setVisibility(View.VISIBLE);
                }
            });
        }

        public void orderByColumn(String column) {
            if(orderBy == column) {
                asc = !asc;
            }
            else {
                asc = false;
            }
            orderBy = column;

            PokemonDAO pokemonDAO =  PokemonDAO.getInstance(getApplicationContext());
            List<Pokemon> pokemons = pokemonDAO.catchThemAll(orderBy, asc);


            pokemonContent.ITEMS.removeAll(pokemonContent.ITEMS);

            for (Pokemon pokemon : pokemons) {
                pokemonContent.ITEMS.add(pokemon);
                pokemonContent.ITEM_MAP.put(Integer.toString(pokemon.number), pokemon);
            }

            adapter.notifyDataSetChanged();
        }



        @Override
        public int getItemCount() {
            return mValues.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            public final View mView;
            public final TextView mIdView;
            public final TextView mNameView;
            public final TextView mListValue1View;
            public final TextView mListValue2View;
            public final ImageView mImageEgg;
            public Pokemon mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mNameView = (TextView) view.findViewById(R.id.name);
                mListValue1View = (TextView) view.findViewById(R.id.listvalue1);
                mListValue2View = (TextView) view.findViewById(R.id.listvalue2);
                mImageEgg = (ImageView) view.findViewById(R.id.ListImageEgg);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mNameView.getText() + "'";
            }
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        TextView value1Titulo = (TextView) findViewById(R.id.value1Titulo);
        TextView value2Titulo = (TextView) findViewById(R.id.value2Titulo);
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        hideArrows();
        value2Titulo.setVisibility(View.VISIBLE);
        if (id == R.id.nav_cp_hp) {
            showLayout = NAV_CP_HP;
            value1Titulo.setText(R.string.maxcp);
            value2Titulo.setText(R.string.maxhp);
            catchThemAll();

        } else if (id == R.id.nav_atk_def) {
            showLayout = NAV_ATK_DEF;
            value1Titulo.setText(R.string.attack);
            value2Titulo.setText(R.string.defense);
            catchThemAll();

        } else if (id == R.id.nav_candies) {
            showLayout = NAV_CANDIES;
            value1Titulo.setText(R.string.candies);
            value2Titulo.setText(R.string.maxcp);
            catchThemAllWhere(Pokemon.Colunas.EVOLUTION+" IS NULL AND "+Pokemon.Colunas.CAPTURERATE+" > 0");

        } else if (id == R.id.nav_eggs) {
            showLayout = NAV_KMEGG;
            value1Titulo.setText(R.string.kms);
            value2Titulo.setVisibility(View.GONE);
            catchThemAllWhere(Pokemon.Colunas.KMEGG+" IS NOT NULL");
        }
        View recyclerView = findViewById(R.id.pokemon_list);
        assert recyclerView != null;
        adapter = setupRecyclerView((RecyclerView) recyclerView);


        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void orderByColumnWhere(String where, String column) {
        if(orderBy == column) {
            asc = !asc;
        }
        else {
            asc = false;
        }
        orderBy = column;

        PokemonDAO pokemonDAO =  PokemonDAO.getInstance(getApplicationContext());
        List<Pokemon> pokemons = pokemonDAO.catchThemAllWhere(where, orderBy, asc);


        pokemonContent.ITEMS.removeAll(pokemonContent.ITEMS);

        for (Pokemon pokemon : pokemons) {
            pokemonContent.ITEMS.add(pokemon);
            pokemonContent.ITEM_MAP.put(Integer.toString(pokemon.number), pokemon);
        }

        adapter.notifyDataSetChanged();
    }

    public void catchThemAll() {
        PokemonDAO pokemonDAO =  PokemonDAO.getInstance(getApplicationContext());
        List<Pokemon> pokemons = pokemonDAO.catchThemAll();


        pokemonContent.ITEMS.removeAll(pokemonContent.ITEMS);

        for (Pokemon pokemon : pokemons) {
            pokemonContent.ITEMS.add(pokemon);
            pokemonContent.ITEM_MAP.put(Integer.toString(pokemon.number), pokemon);
        }

        adapter.notifyDataSetChanged();
    }

    public void catchThemAllWhere(String where) {
        PokemonDAO pokemonDAO =  PokemonDAO.getInstance(getApplicationContext());
        List<Pokemon> pokemons = pokemonDAO.catchThemAllWhere(where);


        pokemonContent.ITEMS.removeAll(pokemonContent.ITEMS);

        for (Pokemon pokemon : pokemons) {
            pokemonContent.ITEMS.add(pokemon);
            pokemonContent.ITEM_MAP.put(Integer.toString(pokemon.number), pokemon);
        }

        adapter.notifyDataSetChanged();
    }

    public void hideArrows() {
        ImageView imgArrowNumber = (ImageView) findViewById(R.id.arrowNumber);
        ImageView imgArrowName = (ImageView) findViewById(R.id.arrowName);
        ImageView imgArrowMaxCp = (ImageView) findViewById(R.id.arrowMaxCp);
        ImageView imgArrowMaxHp = (ImageView) findViewById(R.id.arrowMaxHp);

        imgArrowNumber.setVisibility(View.INVISIBLE);
        imgArrowName.setVisibility(View.INVISIBLE);
        imgArrowMaxCp.setVisibility(View.INVISIBLE);
        imgArrowMaxHp.setVisibility(View.INVISIBLE);
    }
}
