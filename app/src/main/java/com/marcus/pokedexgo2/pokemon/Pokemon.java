package com.marcus.pokedexgo2.pokemon;

import android.content.ContentValues;

public class Pokemon {
    public long id;
    public Integer number;
    public String name;
    public Integer maxCp;
    public Integer maxHp;
    public Integer baseAtk;
    public Integer baseDef;
    public Integer baseStamina;
    public Integer captureRate;
    public Integer fleeRate;
    public Integer kmEgg;
    public Integer candies;
    public Integer total_candies;
    public Double rarity;
    public Integer type1;
    public Integer type2;
    public Integer evolution;
    public Integer preEvolution;

    public static interface Colunas {
        static final String ROWID = "_id";
        static final String NUMBER = "number";
        static final String NAME = "name";
        static final String MAXCP = "max_cp";
        static final String MAXHP = "max_hp";
        static final String BASEATK = "base_atk";
        static final String BASEDEF = "base_def";
        static final String BASESTAMINA = "base_stamina";
        static final String CAPTURERATE = "capture_rate";
        static final String FLEERATE = "flee_rate";
        static final String KMEGG = "kmeggs";
        static final String CANDIES = "candies";
        static final String TOTAL_CANDIES = "total_candies";
        static final String RARITY = "rarity";
        static final String TYPE1 = "type1";
        static final String TYPE2 = "type2";
        static final String EVOLUTION = "evolution";
        static final String PREEVOLUTION = "pre_evolution";

    }

    public Pokemon() {

    }


    public Pokemon(ContentValues valores) {
        this.number = valores.getAsInteger(Colunas.NUMBER);
        this.name = valores.getAsString(Colunas.NAME);
        this.maxCp = valores.getAsInteger(Colunas.MAXCP);
        this.maxHp = valores.getAsInteger(Colunas.MAXHP);
        this.baseAtk = valores.getAsInteger(Colunas.BASEATK);
        this.baseDef = valores.getAsInteger(Colunas.BASEDEF);
        this.baseStamina = valores.getAsInteger(Colunas.BASESTAMINA);
        this.captureRate = valores.getAsInteger(Colunas.CAPTURERATE);
        this.fleeRate = valores.getAsInteger(Colunas.FLEERATE);
        this.kmEgg = valores.getAsInteger(Colunas.KMEGG);
        this.candies = valores.getAsInteger(Colunas.CANDIES);
        this.total_candies = valores.getAsInteger(Colunas.TOTAL_CANDIES);
        this.rarity = valores.getAsDouble(Colunas.RARITY);
        this.type1 = valores.getAsInteger(Colunas.TYPE1);
        this.type2 = valores.getAsInteger(Colunas.TYPE2);
        this.evolution = valores.getAsInteger(Colunas.EVOLUTION);
        this.preEvolution = valores.getAsInteger(Colunas.PREEVOLUTION);
    }

    public Pokemon(final Integer number, final String name, final Integer maxCp, final Integer maxHp,
                   final Integer baseAtk, final Integer baseDef, final Integer baseStamina, final Integer captureRate,
                   final Integer fleeRate, final Integer kmEgg, final Integer candies, final Integer total_candies, final Double rarity, final Integer type1,
                   final Integer type2, final Integer evolution, final Integer preEvolution) {
        this.number = number;
        this.name = name;
        this.maxCp = maxCp;
        this.maxHp = maxHp;
        this.baseAtk = baseAtk;
        this.baseDef = baseDef;
        this.baseStamina = baseStamina;
        this.captureRate = captureRate;
        this.fleeRate = fleeRate;
        this.kmEgg = kmEgg;
        this.candies = candies;
        this.total_candies = total_candies;
        this.rarity = rarity;
        this.type1 = type1;
        this.type2 = type2;
        this.evolution = evolution;
        this.preEvolution = preEvolution;
    }

    public Pokemon(final Integer number, final String name, final Integer maxCp, final Integer maxHp,
                   final Integer baseAtk, final Integer baseDef, final Integer baseStamina, final Integer captureRate,
                   final Integer fleeRate, final Integer kmEgg, final Integer candies, final Integer total_candies, final Integer rarity,
                   final Integer type1, final Integer type2, final Integer evolution, final Integer preEvolution) {
        this.number = number;
        this.name = name;
        this.maxCp = maxCp;
        this.maxHp = maxHp;
        this.baseAtk = baseAtk;
        this.baseDef = baseDef;
        this.baseStamina = baseStamina;
        this.kmEgg = kmEgg;
        this.candies = candies;
        this.total_candies = total_candies;
        this.captureRate = captureRate;
        this.fleeRate = fleeRate;
        this.type1 = type1;
        this.type2 = type2;
        this.evolution = evolution;
        this.preEvolution = preEvolution;

        if(rarity != null) {
            this.rarity = Double.parseDouble(String.valueOf(rarity));
        }
    }


    public ContentValues toContentValues() {
        ContentValues values = new ContentValues();
        values.put(Colunas.NUMBER, this.number);
        values.put(Colunas.NAME, this.name);
        values.put(Colunas.MAXCP, this.maxCp);
        values.put(Colunas.MAXHP, this.maxHp);
        values.put(Colunas.BASEATK, this.baseAtk);
        values.put(Colunas.BASEDEF, this.baseDef);
        values.put(Colunas.BASESTAMINA, this.baseStamina);
        values.put(Colunas.CAPTURERATE, this.captureRate);
        values.put(Colunas.FLEERATE, this.fleeRate);
        values.put(Colunas.KMEGG, this.kmEgg);
        values.put(Colunas.CANDIES, this.candies);
        values.put(Colunas.TOTAL_CANDIES, this.total_candies);
        values.put(Colunas.RARITY, this.rarity);
        values.put(Colunas.TYPE1, this.type1);
        values.put(Colunas.TYPE2, this.type2);
        values.put(Colunas.EVOLUTION, this.evolution);
        values.put(Colunas.PREEVOLUTION, this.preEvolution);
        return values;
    }

}