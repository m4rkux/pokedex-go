package com.marcus.pokedexgo2.pokemon;

import android.content.Context;

import com.google.android.gms.common.api.Api;
import com.marcus.pokedexgo2.database.PokemonDAO;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for providing sample content for user interfaces created by
 * Android template wizards.
 * <p/>
 * TODO: Replace all uses of this class before publishing your app.
 */
public class PokemonContent {

    private Context context;
    public static final List<Pokemon> ITEMS = new ArrayList<>();
    public static final Map<String, Pokemon> ITEM_MAP = new HashMap<String, Pokemon>();



    public PokemonContent(Context context) {
        this.context = context;
        PokemonDAO pokemonDAO =  PokemonDAO.getInstance(context);
        List<Pokemon> pokemons = pokemonDAO.catchThemAll();

        ITEMS.removeAll(ITEMS);
        ITEM_MAP.remove(ITEM_MAP);

        for (Pokemon pokemon : pokemons) {
            ITEMS.add(pokemon);
            ITEM_MAP.put(Integer.toString(pokemon.number), pokemon);
        }
    }
}
