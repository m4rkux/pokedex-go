package com.marcus.pokedexgo2.pokemon;

public enum PokemonType {
    GRASS("#A5D6A7", "#4CAF50", 1),
    FIRE("#EF9A9A", "#F44336", 2),
    WATER("#90CAF9", "#2196F3", 3),
    BUG("#E6EE9C", "#C0CA33", 4),
    FLYING("#80DEEA", "#00BCD4", 5),
    POISON("#B39DDB", "#673AB7", 6),
    NORMAL("#FFE082", "#FFC107", 7),
    ELECTRIC("#FFF59D", "#FBC02D", 8),
    GROUND("#BCAAA4", "#795548", 9),
    FAIRY("#F48FB1", "#E91E63", 10),
    FIGHTING("#FFCC80", "#FF9800", 11),
    ROCK("#BDBDBD", "#9E9E9E", 12),
    PSYCHIC("#9FA8DA", "#3F51B5", 13),
    STEEL("#B0BEC5", "#607D8B", 14),
    GHOST("#7986CB", "#303F9F", 15),
    DRAGON("#FFAB91", "#FF5722", 16),
    ICE("#81D4FA", "#03A9F4", 17);

    private final String lightColor;
    private final String darkColor;
    private final int number;

    PokemonType(String lightColor, String darkColor, int number) {
        this.lightColor = lightColor;
        this.darkColor = darkColor;
        this.number = number;
    }

    public static PokemonType getByNumber(int number) {
        for(PokemonType type : values()) {
            if(type.number == number) {
                return type;
            }
        }
        return null;
    }

    public String getLightColor() {
        return lightColor;
    }

    public String getDarkColor() { return darkColor; }

    public int getNumber() {
        return number;
    }
}
