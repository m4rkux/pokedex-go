package com.marcus.pokedexgo2.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.marcus.pokedexgo2.pokemon.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class PokemonDBHelper extends SQLiteOpenHelper{

    public static final int DATABASE_VERSION = 3;
    public static final String DATABASE_NAME = "pokedexgo.db";

    public PokemonDBHelper(final Context context) {
        super(context, DATABASE_NAME , null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(final SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE_POKEMONS);
        populate(sqLiteDatabase);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase sqLiteDatabase, final int i, final int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_POKEMONS );
        onCreate(sqLiteDatabase);
    }

    // Database properties
    private static final String DATABASE_TABLE_POKEMONS = "pokemons";

    //  Table Pokemons properties
    public static final String KEY_ROWID = "_id";
    public static final String KEY_NUMBER = "number";
    public static final String KEY_NAME = "name";
    public static final String KEY_TYPE = "name";
    public static final String KEY_COLOR = "color";
    public static final String KEY_MAXCP = "max_cp";
    public static final String KEY_MAXHP = "max_hp";
    public static final String KEY_BASEATK = "base_atk";
    public static final String KEY_BASEDEF = "base_def";
    public static final String KEY_BASESTAMINA = "base_stamina";
    public static final String KEY_CAPTURERATE = "capture_rate";
    public static final String KEY_FLEERATE = "flee_rate";
    public static final String KEY_KMEGG = "kmeggs";
    public static final String KEY_CANDIES = "candies";
    public static final String KEY_TOTAL_CANDIES = "total_candies";
    public static final String KEY_RARITY = "rarity";
    public static final String KEY_TYPE1 = "type1";
    public static final String KEY_TYPE2 = "type2";
    public static final String KEY_EVOLUTION = "evolution";
    public static final String KEY_PREEVOLUTION = "pre_evolution";
    private static final String TAG = "DBAdapter";

    // Create Script
    private static final String DATABASE_CREATE_POKEMONS = "CREATE TABLE IF NOT EXISTS " + DATABASE_TABLE_POKEMONS
            + "( " + KEY_ROWID + " INTEGER PRIMARY KEY, "
            + KEY_NUMBER + " INTEGER, "
            + KEY_NAME + " TEXT, "
            + KEY_MAXCP + " INTEGER, "
            + KEY_MAXHP + " INTEGER, "
            + KEY_BASEATK + " INTEGER, "
            + KEY_BASEDEF + " INTEGER, "
            + KEY_BASESTAMINA + " INTEGER, "
            + KEY_CAPTURERATE + " REAL, "
            + KEY_FLEERATE + " REAL, "
            + KEY_KMEGG + " INTEGER, "
            + KEY_CANDIES + " INTEGER, "
            + KEY_TOTAL_CANDIES + " INTEGER, "
            + KEY_RARITY + " REAL, "
            + KEY_TYPE1 + " INTEGER, "
            + KEY_TYPE2 + " INTEGER,"
            + KEY_EVOLUTION + " INTEGER,"
            + KEY_PREEVOLUTION + " INTEGER);";

    private void populate(SQLiteDatabase db) {


        /****************** POKÉMONS ******************/
        /***********************************************************************************************************************/
        List<Pokemon> pokemons = new ArrayList<>();
        pokemons.add(new Pokemon(1, "Bulbasaur", 1071, 82, 126, 126, 90, 16, 10, 2, 25, 0, 1.18, 1, 6, 2, null));
        pokemons.add(new Pokemon(2, "Ivysaur", 1632, 106, 156, 158, 120, 8, 7, null, 100, 25, 0.03, 1, 6, 3, 1));
        pokemons.add(new Pokemon(3, "Venusaur", 2580, 138, 198, 200, 160, 4, 5, null, null, 125, 0.01, 1, 6, null, 2));
        pokemons.add(new Pokemon(4, "Charmander", 955, 73, 128, 108, 78, 16, 10, 2, 25, 0, 0.23, 2, null, 5, null));
        pokemons.add(new Pokemon(5, "Charmeleon", 1557, 103, 160, 140, 116, 8, 7, null, 100, 25, 0.03, 2, null, 6, 4));
        pokemons.add(new Pokemon(6, "Charizard", 2602, 135, 212, 182, 156, 4, 5, null, null, 125, 0.01, 2, 5, null, 5));
        pokemons.add(new Pokemon(7, "Squirtle", 1008, 81, 112, 142, 88, 16, 10, 2, 25, 0, 0.56, 3, null, 8, null));
        pokemons.add(new Pokemon(8 , "Wartortle", 1582, 105, 144, 176, 118, 8, 7, null, 100, 25, 0.03, 3, null, 9, 7));
        pokemons.add(new Pokemon(9, "Blastoise", 2542, 136, 186, 222, 158, 4, 5, null, null, 125, 0.01, 3, null, null, 8));
        pokemons.add(new Pokemon(10, "Caterpie", 443, 82, 62, 66, 90, 40, 20, 2, 12, 0, 3.78, 4, null, 11, null));
        pokemons.add(new Pokemon(11, "Metapod", 477, 90, 56, 86, 100, 20, 9, null, 50, 12, 0.20, 4, null, 12, 10));
        pokemons.add(new Pokemon(12, "Butterfree", 1454, 106, 144, 144, 120, 10, 6, null, null, 62, 0.01, 4, 5, null, 11));
        pokemons.add(new Pokemon(13, "Weedle", 449, 75, 68, 64, 80, 40, 20, 2, 12, 0, 6.61, 4, 6, 14, null));
        pokemons.add(new Pokemon(14, "Kakuna", 485, 82, 62, 82, 90, 20, 9, null, 50, 12, 0.56, 4, 6, 15, 13));
        pokemons.add(new Pokemon(15, "Beedrill", 1439, 114, 144, 130, 130, 10, 6, null, null, 62, 0.07, 4, 6, null, 14));
        pokemons.add(new Pokemon(16, "Pidgey", 679, 75, 94, 90, 80, 40, 20, 2, 12, 0, 18.68, 7, 5, 14, null));
        pokemons.add(new Pokemon(17, "Pidgeotto", 1223, 111, 126, 122, 126, 20, 9, null, 50, 12, 1.15, 7, 5, 18, 16));
        pokemons.add(new Pokemon(18, "Pidgeot", 2091, 143, 170, 166, 166, 10, 6, null, null, 62, 0.26, 7, 5, null, 17));
        pokemons.add(new Pokemon(19, "Rattata", 581, 59, 92, 86, 60, 40, 20, 2, 25, 0, 13.13, 7, null, 20, null));
        pokemons.add(new Pokemon(20, "Raticate", 1444, 98, 146, 150, 110, 16, 7, null, null, 25, 0.23, 7, null, null, 19));
        pokemons.add(new Pokemon(21, "Spearow", 686, 75, 102, 78, 80, 40, 15, 2, 50, 0, 3.78, 7, 5, 22, null));
        pokemons.add(new Pokemon(22, "Fearow", 1746, 114, 168, 146, 130, 16, 7, null, null, 50, 0.10, 7, 5, null, 21));
        pokemons.add(new Pokemon(23, "Ekans", 824, 67, 112, 112, 70, 40, 15, 5, 50, 0, 2.47, 6, null, 24, null));
        pokemons.add(new Pokemon(24, "Arbok", 1767, 106, 166, 166, 120, 16, 7, null, null, 50, 0.10, 6, null, null, 23));
        pokemons.add(new Pokemon(25, "Pikachu", 887, 67, 124, 108, 70, 16, 10, 2, 50, 0, 0.03, 8, null, 26, null));
        pokemons.add(new Pokemon(26, "Raichu", 2028, 106, 200, 154, 120, 8, 6, null, null, 50, 0.01, 8, null, null, 25));
        pokemons.add(new Pokemon(27, "Sandshrew", 798, 90, 90, 144, 100, 40, 10, 5, 50, 0, 1.12, 9, null, 28, null));
        pokemons.add(new Pokemon(28, "Sandslash", 1810, 130, 150, 172, 150, 16, 6, null, null, 50, 0.03, 9, null, null, 27));
        pokemons.add(new Pokemon(29, "Nidoran(F)", 876, 98, 100, 104, 110, 40, 15, 5, 25, 0, 1.35, 6, null, 30, null));
        pokemons.add(new Pokemon(30, "Nidorina", 1404, 122, 132, 136, 140, 20, 7, null, 100, 25, 0.10, 6, null, 30, 29));
        pokemons.add(new Pokemon(31, "Nidoqueen", 2485, 154, 184, 190, 180, 10, 5, null, null, 125, 0.01, 6, 9, null, 30));
        pokemons.add(new Pokemon(32, "Nidoran(M)", 843, 84, 110, 94, 92, 40, 15, 5, 25, 0, 1.58, 6, null, 33, null));
        pokemons.add(new Pokemon(33, "Nidorino", 1372, 108, 142, 128, 122, 20, 7, null, 100, 25, 0.03, 6, null, 34, 32));
        pokemons.add(new Pokemon(34, "Nidoking", 2475, 139, 204, 170, 162, 10, 5, null, null, 125, 0.01, 6, 9, null, 33));
        pokemons.add(new Pokemon(35, "Clefairy", 1200, 122, 116, 124, 140, 24, 10, 2, 50, 0, 1.51, 10, null, 36, null));
        pokemons.add(new Pokemon(36, "Clefable", 2397, 162, 178, 178, 190, 8, 6, null, null, 50, 0.01, 10, null, null, 35));
        pokemons.add(new Pokemon(37, "Vulpix", 931, 71, 106, 118, 76, 24, 10, 5, 50, 0, 0.20, 2, null, 38, null));
        pokemons.add(new Pokemon(38, "Ninetales", 2188, 127, 176, 194, 146, 8, 6, null, null, 50, 0.01, 2, null, null, 37));
        pokemons.add(new Pokemon(39, "Jigglypuff", 917, 193, 98, 54, 230, 40, 10, 2, 50, 0, 0.33, 7, 10, 40, null));
        pokemons.add(new Pokemon(40, "Wigglytuff", 2177, 233, 168, 108, 280, 16, 6, null, null, 50, 0.01, 7, 10, null, 39));
        pokemons.add(new Pokemon(41, "Zubat", 642, 75, 88, 90, 80, 40, 20, 2, 50, 0, 17.27, 6, 5, 42, null));
        pokemons.add(new Pokemon(42, "Golbat", 1921, 130, 164, 164, 150, 16, 7, null, null, 50, 0.66, 6, 5, null, 41));
        pokemons.add(new Pokemon(43, "Oddish", 1148, 82, 134, 130, 90, 48, 15, 5, 25, 0, 1.22, 1, 6, 44, null));
        pokemons.add(new Pokemon(44, "Gloom", 1689, 106, 162, 158, 120, 24, 7, null, 100, 25, 0.03, 1, 6, 45, 43));
        pokemons.add(new Pokemon(45, "Vileplume", 2492, 130, 202, 190, 150, 12, 5, null, null, 125, 0.01, 1, 6, null, 44));
        pokemons.add(new Pokemon(46, "Paras", 916, 67, 122, 120, 70, 32, 15, 5, 50, 0, 2.57, 4, 1, 47, null));
        pokemons.add(new Pokemon(47, "Parasect", 1747, 106, 162, 170, 120, 16, 7, null, null, 50, 0.13, 4, 1, null, 46));
        pokemons.add(new Pokemon(48, "Venonat", 1029, 106, 108, 118, 120, 40, 15, 5, 50, 0, 3.26, 4, 6, 49, null));
        pokemons.add(new Pokemon(49, "Venomoth", 1890, 122, 172, 154, 140, 16, 7, null, null, 50, 0.07, 4, 6, null, 48));
        pokemons.add(new Pokemon(50, "Diglett", 456, 27, 108, 86, 20, 40, 10, 5, 50, 0, 0.30, 9, null, 51, null));
        pokemons.add(new Pokemon(51, "Dugtrio", 1168, 67, 148, 140, 70, 16, 6, null, null, 50, 0.01, 9, null, null, 50));
        pokemons.add(new Pokemon(52, "Meowth", 756, 75, 104, 94, 80, 45, 15, 5, 50, 0, 0.39, 7, null, 53, null));
        pokemons.add(new Pokemon(53, "Persian", 1631, 114, 156, 146, 130, 16, 7, null, null, 50, 0.03, 7, null, null, 52));
        pokemons.add(new Pokemon(54, "Psyduck", 1109, 90, 132, 112, 100, 40, 10, 5, 50, 0, 0.76, 3, null, 55, null));
        pokemons.add(new Pokemon(55, "Golduck", 2386, 138, 194, 176, 160, 16, 6, null, null, 50, 0.03, 3, null, null, 54));
        pokemons.add(new Pokemon(56, "Mankey", 878, 75, 122, 96, 80, 40, 10, 5, 50, 0, 0.03, 11, null, 57, null));
        pokemons.add(new Pokemon(57, "Primeape", 1864, 114, 178, 150, 130, 16, 6, null, null, 50, 0.01, 11, null, null, 56));
        pokemons.add(new Pokemon(58, "Growlithe", 1335, 98, 156, 110, 110, 24, 10, 5, 50, 0, 0.92, 2, null, 59, null));
        pokemons.add(new Pokemon(59, "Arcanine", 2983, 154, 230, 180, 180, 8, 6, null, null, 50, 0.01, 2, null, null, 58));
        pokemons.add(new Pokemon(60, "Poliwag", 795, 75, 108, 98, 80, 40, 15, 5, 25, 0, 1.22, 3, null, 61, null));
        pokemons.add(new Pokemon(61, "Poliwhirl", 1340, 114, 132, 132, 130, 20, 7, null, 100, 25, 0.16, 3, null, 62, 60));
        pokemons.add(new Pokemon(62, "Poliwrath", 2505, 154, 180, 202, 180, 10, 5, null, null, 125, 0.01, 3, 11, null, 61));
        pokemons.add(new Pokemon(63, "Abra", 600, 51, 110, 76, 50, 40, 99, 5, 25, 0, 0.36, 13, null, 64, null));
        pokemons.add(new Pokemon(64, "Kadabra", 1131, 75, 150, 112, 80, 20, 7, null, 100, 25, 0.01, 13, null, 65, 63));
        pokemons.add(new Pokemon(65, "Alakazam", 1813, 98, 186, 152, 110, 10, 5, null, null, 125, 0.01, 13, null, null, 64));
        pokemons.add(new Pokemon(66, "Machop", 1089, 122, 118, 96, 140, 40, 10, 5, 25, 0, 0.46, 11, null, 67, null));
        pokemons.add(new Pokemon(67, "Machoke", 1760, 138, 154, 144, 160, 20, 7, null, 100, 25, 0.01, 11, null, 68, 66));
        pokemons.add(new Pokemon(68, "Machamp", 2594, 154, 198, 180, 180, 10, 5, null, null, 125, 0.01, 11, null, null, 67));
        pokemons.add(new Pokemon(69, "Bellsprout", 1117, 90, 158, 78, 100, 40, 15, 5, 25, 0, 1.64, 1, 6, 70, null));
        pokemons.add(new Pokemon(70, "Weepinbell", 1723, 114, 190, 110, 130, 20, 7, null, 100, 25, 0.10, 1, 6, 71, 69));
        pokemons.add(new Pokemon(71, "Victreebel", 2530, 138, 222, 152, 160, 10, 5, null, null, 125, 0.01, 1, 6, null, 70));
        pokemons.add(new Pokemon(72, "Tentacool", 905, 75, 106, 136, 80, 40, 15, 5, 50, 0, 0.33, 3, 6, 73, null));
        pokemons.add(new Pokemon(73, "Tentacruel", 2220, 138, 170, 196, 160, 16, 7, null, null, 50, 0.03, 3, 6, null, 72));
        pokemons.add(new Pokemon(74, "Geodude", 849, 75, 106, 118, 80, 40, 10, 2, 25, 0, 0.95, 12, 9, 75, null));
        pokemons.add(new Pokemon(75, "Graveler", 1433, 98, 142, 156, 110, 20, 7, null, 100, 25, 0.13, 12, 9, 76, 74));
        pokemons.add(new Pokemon(76, "Golem", 2303, 138, 176, 198, 160, 10, 5, null, null, 125, 0.01, 12, 9, null, 75));
        pokemons.add(new Pokemon(77, "Ponyta", 1516, 90, 168, 138, 100, 32, 10, 5, 50, 0, 0.46, 2, null, 78, null));
        pokemons.add(new Pokemon(78, "Rapidash", 2199, 114, 200, 170, 130, 12, 6, null, null, 50, 0.01, 2, null, null, 77));
        pokemons.add(new Pokemon(79, "Slowpoke", 1218, 154, 110, 110, 180, 40, 12, 5, 50, 0, 0.23, 3, 13, 80, null));
        pokemons.add(new Pokemon(80, "Slowbro", 2597, 162, 184, 194, 190, 16, 6, null, null, 50, 0.01, 3, 13, null, 79));
        pokemons.add(new Pokemon(81, "Magnemite", 890, 51, 128, 138, 50, 40, 10, 5, 50, 0, 0.30, 8, 14, 82, null));
        pokemons.add(new Pokemon(82, "Magneton", 1879, 90, 186, 180, 100, 16, 6, null, null, 50, 0.01, 8, 14, null, 81));
        pokemons.add(new Pokemon(83, "Farfetch'd", 1263, 94, 138, 132, 104, 24, 9, 5, null, 0, 0.01, 7, 5, null, null));
        pokemons.add(new Pokemon(84, "Doduo", 855, 67, 126, 96, 70, 40, 10, 5, 50, 0, 5.94, 7, 5, 85, null));
        pokemons.add(new Pokemon(85, "Dodrio", 1836, 106, 182, 150, 120, 16, 6, null, null, 50, 0.20, 7, 5, null, 84));
        pokemons.add(new Pokemon(86, "Seel", 1107, 114, 104, 138, 130, 40, 9, 5, 50, 0, 0.01, 3, null, 87, null));
        pokemons.add(new Pokemon(87, "Dewgong", 2145, 154, 156, 192, 150, 16, 6, null, null, 50, 0.01, 3, 17, null, 86));
        pokemons.add(new Pokemon(88, "Grimer", 1284, 138, 124, 110, 160, 40, 10, 5, 50, 0, 0.01, 6, null, 89, null));
        pokemons.add(new Pokemon(89, "Muk", 2602, 177, 180, 188, 210, 16, 6, null, null, 50, 0.01, 6, null, null, 88));
        pokemons.add(new Pokemon(90, "Shellder", 822, 59, 120, 122, 60, 40, 10, 5, 50, 0, 0.13, 3, null, 91, null));
        pokemons.add(new Pokemon(91, "Cloyster", 2052, 90, 196, 196, 100, 16, 6, null, null, 50, 0.01, 3, 17, null, 90));
        pokemons.add(new Pokemon(92, "Gastly", 804, 59, 136, 82, 60, 32, 10, 5, 25, 0, 0.01, 15, 6, 93, null));
        pokemons.add(new Pokemon(93, "Haunter", 1380, 82, 172, 118, 90, 16, 7, null, 100, 25, 0.01, 15, 6, 94, 92));
        pokemons.add(new Pokemon(94, "Gengar", 2078, 106, 204, 156, 120, 8, 5, null, null, 125, 0.01, 15, 6, null, 93));
        pokemons.add(new Pokemon(95, "Onix", 857, 67, 90, 186, 70, 16, 9, 10, null, 0, 0.01, 12, 9, null, null));
        pokemons.add(new Pokemon(96, "Drowzee", 1075, 106, 104, 140, 120, 40, 10, 5, 50, 0, 0.01, 13, null, 97, null));
        pokemons.add(new Pokemon(97, "Hypno", 2184, 146, 162, 196, 170, 16, 6, null, null, 50, 0.01, 13, null, null, 96));
        pokemons.add(new Pokemon(98, "Krabby", 792, 59, 116, 110, 60, 40, 15, 5, 50, 0, 0.01, 3, null, 99, null));
        pokemons.add(new Pokemon(99, "Kingler", 1823, 98, 178, 168, 110, 16, 7, null, null, 50, 0.01, 3, null, null, 98));
        pokemons.add(new Pokemon(100, "Voltorb", 839, 75, 102, 124, 180, 40, 10, 5, 50, 0, 0.01, 8, null, 101, null));
        pokemons.add(new Pokemon(101, "Electrode", 1646, 106, 150, 174, 120, 16, 6, null, null, 50, 0.01, 8, null, null, 100));
        pokemons.add(new Pokemon(102, "Exeggcute", 1099, 106, 110, 132, 120, 40, 10, 5, 50, 0, 0.01, 1, 13, 103, null));
        pokemons.add(new Pokemon(103, "Exeggutor", 2955, 162, 232, 164, 190, 16, 6, null, null, 50, 0.01, 1, 13, null, 102));
        pokemons.add(new Pokemon(104, "Cubone", 1006, 90, 102, 150, 100, 32, 10, 5, 50, 0, 0.01, 9, null, 105, null));
        pokemons.add(new Pokemon(105, "Marowak", 1656, 106, 140, 202, 120, 12, 6, null, null, 50, 0.01, 9, null, null, 104));
        pokemons.add(new Pokemon(106, "Hitmonlee", 1492, 90, 148, 172, 100, 16, 9, 10, null, 0, 0.01, 11, null, null, null));
        pokemons.add(new Pokemon(107, "Hitmonchan", 1516, 90, 138, 204, 100, 16, 9, 10, null, 0, 0.01, 11, null, null, null));
        pokemons.add(new Pokemon(108, "Lickitung", 1626, 154, 126, 160, 180, 16, 9, 5, null, 0, 0.01, 7, null, null, null));
        pokemons.add(new Pokemon(109, "Koffing", 1151, 75, 136, 142, 80, 40, 10, 5, 50, 0, 0.01, 6, null, 110, null));
        pokemons.add(new Pokemon(110, "Weezing", 2250, 114, 190, 198, 130, 16, 6, null, null, 50, 0.01, 6, null, null, 109));
        pokemons.add(new Pokemon(111, "Rhyhorn", 1182, 138, 110, 116, 160, 40, 10, 5, 50, 0, 0.01, 9, 12, 112, null));
        pokemons.add(new Pokemon(112, "Rhydon", 2243, 177, 166, 160, 210, 16, 6, null, null, 50, 0.01, 9, 12, null, 111));
        pokemons.add(new Pokemon(113, "Chansey", 675, 407, 40, 60, 500, 16, 9, 10, null, 0, 0.01, 7, null, null, null));
        pokemons.add(new Pokemon(114, "Tangela", 1739, 114, 164, 152, 130, 32, 9, 5, null, 0, 0.01, 1, null, null, null));
        pokemons.add(new Pokemon(115, "Kangaskhan", 2043, 177, 142, 178, 210, 16, 9, 5, null, 0, 0.01, 7, null, null, null));
        pokemons.add(new Pokemon(116, "Horsea", 794, 59, 122, 100, 60, 40, 10, 5, 50, 0, 0.01, 3, null, 117, null));
        pokemons.add(new Pokemon(117, "Seadra", 1713, 98, 176, 150, 110, 16, 6, null, null, 50, 0.01, 3, null, null, 116));
        pokemons.add(new Pokemon(118, "Goldeen", 965, 82, 112, 126, 90, 40, 15, 5, 50, 0, 0.01, 3, null, 119, null));
        pokemons.add(new Pokemon(119, "Seaking", 2043, 138, 172, 160, 160, 16, 7, null, null, 50, 0.01, 3, null, null, 118));
        pokemons.add(new Pokemon(120, "Staryu", 937, 59, 130, 128, 60, 40, 15, 5, 50, 0, 0.01, 3, null, 121, null));
        pokemons.add(new Pokemon(121, "Starmie", 2182, 106, 194, 192, 120, 16, 6, null, null, 50, 0.01, 3, null, null, 120));
        pokemons.add(new Pokemon(122, "Mr. Mime", 1494, 75, 154, 196, 80, 24, 9, 10, null, 0, 0.01, 13, 10, null, null));
        pokemons.add(new Pokemon(123, "Scyther", 2073, 122, 176, 180, 140, 24, 9, 10, null, 0, 0.01, 4, 5, null, null));
        pokemons.add(new Pokemon(124, "Jynx", 1716, 114, 172, 134, 130, 24, 9, 10, null, 0, 0.01, 17, 13, null, null));
        pokemons.add(new Pokemon(125, "Electabuzz", 2119, 114, 198, 160, 130, 24, 9, 10, null, 0, 0.01, 8, null, null, null));
        pokemons.add(new Pokemon(126, "Magmar", 2265, 114, 214, 158, 130, 24, 9, 10, null, 0, 0.01, 2, null, null, null));
        pokemons.add(new Pokemon(127, "Pinsir", 2121, 114, 184, 186, 130, 24, 9, 10, null, 0, 0.01, 4, null, null, null));
        pokemons.add(new Pokemon(128, "Tauros", 1844, 130, 148, 184, 150, 24, 9, 5, null, 0, 0.01, 7, null, null, null));
        pokemons.add(new Pokemon(129, "Magikarp", 262, 43, 42, 84, 40, 56, 15, 2, 400, 0, 0.07, 3, null, 130, null));
        pokemons.add(new Pokemon(130, "Gyarados", 2688, 162, 192, 196, 190, 8, 7, null, null, 400, 0.01, 3, 5, null, 129));
        pokemons.add(new Pokemon(131, "Lapras", 2980, 217, 186, 190, 260, 16, 9, 10, null, 0, 0.01, 3, 17, null, null));
        pokemons.add(new Pokemon(132, "Ditto", 919, 87, 110, 110, 96, 16, 10, null, null, 0, 0.01, 7, null, null, null));
        pokemons.add(new Pokemon(133, "Eevee", 1077, 98, 114, 128, 110, 32, 10, 10, 25, 0, 0.01, 7, null, null, null));
        pokemons.add(new Pokemon(134, "Vaporeon", 2816, 211, 217, 186, 168, 12, 6, null, null, 25, 0.01, 3, null, null, 133));
        pokemons.add(new Pokemon(135, "Jolteon", 2140, 114, 192, 174, 130, 12, 6, null, null, 25, 0.01, 8, null, null, 133));
        pokemons.add(new Pokemon(136, "Flareon", 2643, 114, 238, 178, 130, 12, 6, null, null, 25, 0.01, 2, null, null, 133));
        pokemons.add(new Pokemon(137, "Porygon", 1691, 114, 156, 158, 130, 32, 9, 5, null, 0, 0.01, 7, null, null, null));
        pokemons.add(new Pokemon(138, "Omanyte", 1119, 67, 132, 160, 70, 32, 9, 10, 50, 0, 0.01, 12, 3, null, 139));
        pokemons.add(new Pokemon(139, "Omastar", 2233, 122, 180, 202, 140, 12, 5, null, null, 50, 0.01, 12, 3, null, 138));
        pokemons.add(new Pokemon(140, "Kabuto", 1104, 59, 148, 142, 60, 32, 9, 10, 50, 0, 0.01, 12, 3, 141, null));
        pokemons.add(new Pokemon(141, "Kabutops", 2130, 106, 190, 190, 120, 12, 5, null, null, 50, 0.01, 12, 3, null, 140));
        pokemons.add(new Pokemon(142, "Aerodactyl", 2165, 138, 182, 162, 160, 16, 9, 10, null, 0, 0.01, 12, 5, null, null));
        pokemons.add(new Pokemon(143, "Snorlax", 3112, 264, 180, 180, 320, 16, 9, 10, null, 0, 0.01, 7, null, null, null));
        pokemons.add(new Pokemon(144, "Articuno", 2978, 154, 198, 242, 180, 0, 10, null, null, 0, 0, 17, 5, null, null));
        pokemons.add(new Pokemon(145, "Zapdos", 3114, 154, 232, 194, 180, 0, 10, null, null, 0, 0, 8, 5, null, null));
        pokemons.add(new Pokemon(146, "Moltres", 3240, 154, 242, 194, 180, 0, 10, null, null, 0, 0, 2, 5, null, null));
        pokemons.add(new Pokemon(147, "Dratini", 983, 76, 128, 110, 82, 32, 9, 10, 25, 0, 0.01, 16, null, 147, null));
        pokemons.add(new Pokemon(148, "Dragonair", 1747, 108, 170, 152, 122, 8, 6, null, 100, 25, 0.01, 16, null, 148, 147));
        pokemons.add(new Pokemon(149, "Dragonite", 3500, 155, 250, 212, 182, 4, 5, null, null, 125, 0.01, 16, 5,null, 148));
        pokemons.add(new Pokemon(150, "Mewtwo", 4144, 179, 284, 202, 212, 0, 10, null, null, 0, 0.01, 13, null, null, null));
        pokemons.add(new Pokemon(151, "Mew", 3299, 169, 220, 220, 200, 0, 10, null, null, 0, 0, 13, null, null, null));

        for (Pokemon pokemon: pokemons) {
            db.insert(DATABASE_TABLE_POKEMONS, null, pokemon.toContentValues());
        }
    }
}
