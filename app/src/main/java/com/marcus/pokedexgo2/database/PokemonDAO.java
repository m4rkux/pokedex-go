package com.marcus.pokedexgo2.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;

import com.marcus.pokedexgo2.pokemon.Pokemon;

import java.util.ArrayList;
import java.util.List;

public class PokemonDAO {

    private static final String TABELA = "pokemons";
    private final SQLiteDatabase database;

    //**************************************
    // Logica de singleton
    //**************************************

    private static PokemonDAO instance;

    private PokemonDAO(Context context) {
        database = new PokemonDBHelper(context).getWritableDatabase();
    }

    ;

    public static PokemonDAO getInstance(Context context) {
        if (instance == null) {
            instance = new PokemonDAO(context);
        }

        return instance;
    }

    //**************************************
    // Metodos
    //**************************************

    public long insert(Pokemon pokemon) {
        return database.insert(TABELA, null, pokemon.toContentValues());
    }

    public Pokemon getByNumero(int numero) {
        Pokemon pokemon = null;
        final Cursor cursor = database.query(TABELA, null, Pokemon.Colunas.NUMBER+"= ?", new String[]{String.valueOf(numero)}, null, null, null, null);
        try {
            while (cursor.moveToNext()) {
                ContentValues valores = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, valores);
                pokemon = new Pokemon(valores);
            }
        } finally {
            cursor.close();
        }

        return pokemon;
    }

    public List<Pokemon> catchThemAll() {
        List<Pokemon> pokemons = new ArrayList<>();
        final Cursor cursor = database.query(TABELA, null, null, null, null, null, null, null);
        try {
            while (cursor.moveToNext()) {
                ContentValues valores = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, valores);
                Pokemon pokemon = new Pokemon(valores);
                pokemons.add(pokemon);
            }
        } finally {
            cursor.close();
        }
        return pokemons;
    }

    public List<Pokemon> catchThemAll(String order, boolean asc) {
        List<Pokemon> pokemons = new ArrayList<>();

        if(asc) {
            order = order + " ASC";
        }
        else {
            order = order + " DESC";
        }

        final Cursor cursor = database.query(TABELA, null, null, null, null, null, order, null);
        try {
            while (cursor.moveToNext()) {
                ContentValues valores = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, valores);
                Pokemon pokemon = new Pokemon(valores);
                pokemons.add(pokemon);
            }
        } finally {
            cursor.close();
        }
        return pokemons;
    }


    public List<Pokemon> catchThemAllWhere(String where) {
        List<Pokemon> pokemons = new ArrayList<>();
        final Cursor cursor = database.query(TABELA, null, where, null, null, null, null, null);
        try {
            while (cursor.moveToNext()) {
                ContentValues valores = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, valores);
                Pokemon pokemon = new Pokemon(valores);
                pokemons.add(pokemon);
            }
        } finally {
            cursor.close();
        }
        return pokemons;
    }


    public List<Pokemon> catchThemAllWhere(String where, String order, boolean asc) {
        List<Pokemon> pokemons = new ArrayList<>();

        if(asc) {
            order = order + " ASC";
        }
        else {
            order = order + " DESC";
        }

        final Cursor cursor = database.query(TABELA, null, where, null, null, null, order, null);
        try {
            while (cursor.moveToNext()) {
                ContentValues valores = new ContentValues();
                DatabaseUtils.cursorRowToContentValues(cursor, valores);
                Pokemon pokemon = new Pokemon(valores);
                pokemons.add(pokemon);
            }
        } finally {
            cursor.close();
        }
        return pokemons;
    }


}
