package com.marcus.pokedexgo2;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Handler;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.marcus.pokedexgo2.pokemon.Pokemon;
import com.marcus.pokedexgo2.pokemon.PokemonContent;
import com.marcus.pokedexgo2.pokemon.PokemonType;

/**
 * A fragment representing a single Pokemon detail screen.
 * This fragment is either contained in a {@link PokemonListActivity}
 * in two-pane mode (on tablets) or a {@link PokemonDetailActivity}
 * on handsets.
 */
public class PokemonDetailFragment extends Fragment {
    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String ARG_ITEM_ID = "item_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private Pokemon mItem;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public PokemonDetailFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments().containsKey(ARG_ITEM_ID)) {
            // Load the dummy content specified by the fragment
            // arguments. In a real-world scenario, use a Loader
            // to load content from a content provider.
            mItem = PokemonContent.ITEM_MAP.get(getArguments().getString(ARG_ITEM_ID));

            Activity activity = this.getActivity();
            CollapsingToolbarLayout appBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);

            if (appBarLayout != null) {
                appBarLayout.setTitle(mItem.name);

                PokemonType tipo = PokemonType.getByNumber(mItem.type1);
                appBarLayout.setBackgroundColor(Color.parseColor(tipo.getDarkColor()));
            }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.pokemon_detail, container, false);

        // Show the dummy content as text in a TextView.
        if (mItem != null) {
            ((TextView) rootView.findViewById(R.id.textViewAttackValue)).setText(Integer.toString(mItem.baseAtk));

            ((TextView) rootView.findViewById(R.id.textViewDefenseValue)).setText(Integer.toString(mItem.baseDef));
            ((TextView) rootView.findViewById(R.id.textViewMaxCpValue)).setText(Integer.toString(mItem.maxCp));
            ((TextView) rootView.findViewById(R.id.textViewMaxHpValue)).setText(Integer.toString(mItem.maxHp));
            ((TextView) rootView.findViewById(R.id.textViewFleeRateValue)).setText(Integer.toString(mItem.fleeRate)+"%");
            ((TextView) rootView.findViewById(R.id.textViewCaptureRateValue)).setText(Integer.toString(mItem.captureRate)+"%");
            ((TextView) rootView.findViewById(R.id.textViewRarityalue)).setText(Double.toString(mItem.rarity)+"%");

            if(mItem.candies != null) {
                ((TextView) rootView.findViewById(R.id.textViewCandiesValue)).setText(Integer.toString(mItem.candies));
            }
            else {
                ((TextView) rootView.findViewById(R.id.textViewCandiesValue)).setVisibility(View.GONE);
                ((TextView) rootView.findViewById(R.id.textViewCandies)).setVisibility(View.GONE);
            }

            if(mItem.kmEgg != null) {
                ((TextView) rootView.findViewById(R.id.textViewKmEggValue)).setText(Integer.toString(mItem.kmEgg)+" "+ getString(R.string.kms));
            }
            else {
                ((TextView) rootView.findViewById(R.id.textViewKmEggValue)).setVisibility(View.GONE);
                ((ImageView) rootView.findViewById(R.id.imageViewKmEgg)).setVisibility(View.GONE);
            }

        }

        final View barTotalAttack =  (View) rootView.findViewById(R.id.barTotalAttack);
        final View barTotalDefense =  (View) rootView.findViewById(R.id.barTotalDefense);
        final View barTotalMaxCP =  (View) rootView.findViewById(R.id.barTotalMaxCP);
        final View barTotalMaxHP =  (View) rootView.findViewById(R.id.barTotalMaxHP);
        final double MAX_ATTACK = 284;
        final double MAX_DEFENSE = 242;
        final double MAX_MAXCP = 4144;
        final double MAX_MAXHP = 407;

        barTotalAttack.post(new Runnable() {
            @Override
            public void run() {
                int totalWidth = barTotalAttack.getWidth();
                int height = barTotalAttack.getHeight();
                double percentage = ((double)mItem.baseAtk) / MAX_ATTACK;
                double width =  totalWidth * percentage;

                ((View)rootView.findViewById(R.id.barValueAttack)).setLayoutParams(new RelativeLayout.LayoutParams(new Double(width).intValue(), height));
            }
        });

        barTotalDefense.post(new Runnable() {
            @Override
            public void run() {
                int totalWidth = barTotalDefense.getWidth();
                int height = barTotalDefense.getHeight();
                double percentage = ((double)mItem.baseDef) / MAX_DEFENSE;
                double width =  totalWidth * percentage;

                ((View)rootView.findViewById(R.id.barValueDefense)).setLayoutParams(new RelativeLayout.LayoutParams(new Double(width).intValue(), height));
            }
        });

        barTotalMaxCP.post(new Runnable() {
            @Override
            public void run() {
                int totalWidth = barTotalMaxCP.getWidth();
                int height = barTotalMaxCP.getHeight();
                double percentage = ((double)mItem.maxCp) / MAX_MAXCP;
                double width =  totalWidth * percentage;

                ((View)rootView.findViewById(R.id.barValueMaxCP)).setLayoutParams(new RelativeLayout.LayoutParams(new Double(width).intValue(), height));
            }
        });

        barTotalMaxHP.post(new Runnable() {
            @Override
            public void run() {
                int totalWidth = barTotalMaxHP.getWidth();
                int height = barTotalMaxHP.getHeight();
                double percentage = ((double)mItem.maxHp) / MAX_MAXHP;
                double width =  totalWidth * percentage;

                ((View)rootView.findViewById(R.id.barValueMaxHP)).setLayoutParams(new RelativeLayout.LayoutParams(new Double(width).intValue(), height));
            }
        });



        return rootView;
    }
}
